using System;
using System.Reflection;

using Avalonia.Controls;
using Avalonia.Media;

namespace AionianApp.Views;

public partial class MainWindow : Window
{
	public MainWindow()
	{
		var s = Assembly.GetExecutingAssembly().GetManifestResourceStream("iconfile") ?? throw new ArgumentException("Icon file could not be loaded!");
		// Background = new SolidColorBrush(Color.FromRgb(0x10, 0x10, 0x40));
		// Foreground = new SolidColorBrush(Color.FromUInt32(0xffffffff));
		Title = "Aioninan-App";
		Icon = new(s);
		Content = new MainView();
	}
}