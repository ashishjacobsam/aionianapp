using Aionian;

using AionianApp.Models;

using Avalonia.Controls;
using Avalonia.Controls.Documents;
using Avalonia.Layout;
using Avalonia.Media;

using System;
using System.Collections.Generic;
using System.Linq;

namespace AionianApp.Views;

public class Page_BibleReading : UserControl
{
	public const double TitleByContentSizeRatio = 2;
	internal readonly CoreAionianApp CoreApp;
	internal ChapterwiseBible LoadedBible;
	internal (BibleBook BookEnum, string regionalNames)[] LoadedBibleBooks;
	internal double CurrentFontSize;
	internal TextBlock ChapterTitle;
	internal SelectableTextBlock ChapterContent;
	internal WrapPanel ComboBoxHolder;
	internal ComboBox AvailableBibles;
	internal ComboBox AvailableBooks;
	internal ComboBox AvailableChapter;
	internal static SolidColorBrush TextColor, NumberColor;
	static Page_BibleReading()
	{
		NumberColor = new SolidColorBrush(Color.FromRgb(0x50, 0xf0, 0x10));
		TextColor = new SolidColorBrush(Color.FromRgb(0xf0, 0xf0, 0xf0));
	}
	public Page_BibleReading()
	{
		CoreApp = MainView.CurrentInstance?.CoreApp ?? throw new ArgumentException("Object not initialized!");
		CurrentFontSize = CoreApp.Setting.FontSize;
		LoadedBibleBooks = Array.Empty<(BibleBook BookEnum, string regionalNames)>();
		ChapterTitle = new()
		{
			Text = "No Bibles Available!",
			FontSize = CurrentFontSize * TitleByContentSizeRatio,
			HorizontalAlignment = HorizontalAlignment.Center,
			TextWrapping = TextWrapping.WrapWithOverflow,
		};
		ChapterContent = new()
		{
			Text = "Download bibles from the \"Content\" tab\n",
			HorizontalAlignment = HorizontalAlignment.Stretch,
			VerticalAlignment = VerticalAlignment.Stretch,
		};
		AvailableBibles = AvailableBooks = AvailableChapter = null!;
		ComboBoxHolder = new WrapPanel()
		{
			Orientation = Orientation.Horizontal,
			Margin = new(5),
			HorizontalAlignment = HorizontalAlignment.Center,
		};
		Content = new ScrollViewer()
		{
			Content = new StackPanel()
			{
				Children =
				{
					new Expander()
					{
						Content = ComboBoxHolder,
						HorizontalAlignment = HorizontalAlignment.Stretch,
						ExpandDirection = ExpandDirection.Down,
						Header = new Label()
						{
							Content = "Select Portion",
							HorizontalContentAlignment = HorizontalAlignment.Center
						},
					},
					ChapterTitle,
					ChapterContent,
				}
			},
			Margin = new(5),
			Padding = new(5),
			HorizontalAlignment = HorizontalAlignment.Stretch,
			HorizontalContentAlignment = HorizontalAlignment.Stretch,
			VerticalAlignment = VerticalAlignment.Stretch,
		};
		LoadComboBoxes();
		if (CoreApp.InstalledCount > 0)
		{
			ChapterTitle.Text = "";
			ChapterContent.Text = "";
		}
		UpdateBibles(null, new());
		CoreApp.SettingChanged += ApplySetting;
	}
	public void LoadComboBoxes()
	{

		AvailableBibles = new ComboBox()
		{
			AutoScrollToSelectedItem = true,
			PlaceholderText = "Select Bible",
			HorizontalAlignment = HorizontalAlignment.Center,
			HorizontalContentAlignment = HorizontalAlignment.Center,
		};
		AvailableBooks = new ComboBox()
		{
			AutoScrollToSelectedItem = true,
			PlaceholderText = "Select Book",
			HorizontalAlignment = HorizontalAlignment.Center,
			HorizontalContentAlignment = HorizontalAlignment.Center,
		};
		AvailableChapter = new ComboBox()
		{
			AutoScrollToSelectedItem = true,
			PlaceholderText = "Select Chapter",
			HorizontalAlignment = HorizontalAlignment.Center,
			HorizontalContentAlignment = HorizontalAlignment.Center,
		};
		AvailableBibles.SelectionChanged += BibleSelectionChanged;
		AvailableBooks.SelectionChanged += BookSelectionChanged;
		AvailableChapter.SelectionChanged += ChapterSelectionChanged;
		var childs = ComboBoxHolder.Children;
		childs.Clear();
		childs.Add(AvailableBibles);
		childs.Add(AvailableBooks);
		childs.Add(AvailableChapter);
		UpdateBibles(null, new());
	}
	public void BibleSelectionChanged(object? o, SelectionChangedEventArgs e)
	{
		if (e.Source != AvailableBibles) return;
		var idx = AvailableBibles.SelectedIndex;
		if (idx < 0) return;
		LoadBible(CoreApp.Installed[idx]);
	}
	public void BookSelectionChanged(object? o, SelectionChangedEventArgs e)
	{
		if (e.Source != AvailableBooks) return;
		var idx = AvailableBooks.SelectedIndex;
		if (idx < 0) return;
		LoadBook(LoadedBibleBooks[idx].BookEnum);
	}
	public void ChapterSelectionChanged(object? o, SelectionChangedEventArgs e)
	{
		if (e.Source != AvailableChapter) return;
		int idx = AvailableChapter.SelectedIndex;
		if (idx < 0) return;
		LoadChapter((byte)(idx + 1));
	}
	public void LoadBible(BibleLink link)
	{
		LoadedBible = CoreApp.LoadChapterwiseBible(link);
		AvailableChapter.ItemsSource = null;
		LoadedBibleBooks = LoadedBible.RegionalNames.Select((x) => (x.Key, x.Value)).ToArray();
		AvailableBooks.ItemsSource = LoadedBible.RegionalNames.Values;
	}
	public void LoadBook(BibleBook book)
	{
		if (book == BibleBook.NULL)
			book = LoadedBibleBooks[0].BookEnum;
		AvailableChapter.ItemsSource = Enumerable.Range(1, LoadedBible.GetChapterCount(book)).Select((x) => x.ToString());
	}
	public void LoadChapter(byte chapter)
	{
		Dictionary<byte, string> content = LoadedBible.LoadChapter(LoadedBibleBooks[AvailableBooks.SelectedIndex].BookEnum, chapter);
		Process(content);
	}
	public void Process(IEnumerable<KeyValuePair<byte, string>> chapter)
	{
		ChapterTitle.Text = $"{AvailableBooks.SelectedItem} {AvailableChapter.SelectedItem}";
		ChapterContent.Text = null;
		InlineCollection inlines = new();
		TextWrapping wrap = TextWrapping.Wrap;
		foreach (var item in chapter)
		{
			inlines.Add
			(
				new Run()
				{
					Foreground = NumberColor,
					Text = $"{item.Key} ",
				}
			);
			inlines.Add
			(
				new Run()
				{
					Foreground = TextColor,
					Text = $"{item.Value}\n",
				}
			);
		}
		ChapterContent.Inlines = inlines;
		ChapterContent.TextWrapping = wrap;
		ChapterContent.IsVisible = true;
	}
	public void ApplySetting(object? o, AppSetting.AppSettingArgs e)
	{
		CurrentFontSize = CoreApp.Setting.FontSize;
		ChapterContent.FontSize = CurrentFontSize;
		ChapterTitle.FontSize = CurrentFontSize * TitleByContentSizeRatio;
	}
	public void UpdateBibles(object? o, EventArgs e)
	{
		ResetComboBox(AvailableChapter);
		ResetComboBox(AvailableBooks);
		ResetComboBox(AvailableBibles);
		AvailableBibles.ItemsSource = CoreApp.Installed.Select((x) => x.ToString());
		static void ResetComboBox(ComboBox box)
		{
			box.SelectedIndex = -1;
			box.ItemsSource = null;
		}
		ChapterContent.IsVisible = false;
		ChapterTitle.Text = "";
	}
}