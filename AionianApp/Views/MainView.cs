using Avalonia.Controls;
using Avalonia.Layout;
using AionianApp.Models;

namespace AionianApp.Views;

public class MainView : UserControl
{
	internal static MainView? CurrentInstance = null;
	internal readonly Page_BibleReading Page_BibleReading;
	internal readonly Page_BibleSearch Page_BibleSearch;
	internal readonly Page_ContentManager Page_ContentManager;
	internal readonly Page_Setting Page_Setting;
	internal readonly CoreAionianApp CoreApp;
	internal readonly TabControl CurrentTabs;
	public MainView()
	{
		CurrentInstance = this;
		CoreApp = new();
		double fontsize = 15;
		VerticalAlignment = VerticalAlignment.Top;
		HorizontalAlignment = HorizontalAlignment.Left;
		HorizontalContentAlignment = HorizontalAlignment.Stretch;
		HorizontalAlignment = HorizontalAlignment.Stretch;
		Page_BibleReading = new();
		Page_BibleSearch = new();
		Page_ContentManager = new();
		Page_Setting = new();
		Content = CurrentTabs = new TabControl()
		{
			VerticalAlignment = VerticalAlignment.Center,
			VerticalContentAlignment = VerticalAlignment.Center,
			Items =
			{
				new TabItem()
				{
					Header = "Reading",
					Content = Page_BibleReading,
					FontSize = fontsize,
				},
				new TabItem()
				{
					Header = "Content",
					Content = Page_ContentManager,
					FontSize = fontsize,
				},
				new TabItem()
				{
					Header = "Search",
					Content = Page_BibleSearch,
					FontSize = fontsize
				},
				new TabItem()
				{
					Header = "Settings",
					Content = Page_Setting,
					FontSize = fontsize,
				}
			}
		};
		CurrentTabs.SelectionChanged += OnChanged;
		Page_BibleReading.ApplySetting(null, new());
		(App.Current as App)?.ApplyTheme(CoreApp.Setting.CurrentTheme);
	}
	public void OnChanged(object? o, SelectionChangedEventArgs e)
	{
		if (e.Source == CurrentTabs && CurrentTabs.SelectedIndex == 3)
		{
			Page_Setting.TextsizeSlider.Value = CoreApp.Setting.FontSize;
			Page_Setting.ThemeBox.SelectedIndex = (byte)CoreApp.Setting.CurrentTheme;
		}
	}
}