using AionianApp.Models;

using Avalonia.Controls;
using Avalonia.Layout;
using Avalonia.Input;
using Aionian;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.ObjectModel;
using System.Net.Http.Handlers;
using System.Net.Http;
using Avalonia.Threading;

namespace AionianApp.Views;

public class Page_ContentManager : UserControl
{
	internal readonly ToggleSwitch SeeInstalled;
	internal readonly ProgressBar LoadingBar;
	internal readonly Button RefreshButton, DeleteButton, DownloadButton;
	internal ListBox LinkList;
	internal ObservableCollection<BibleLink> DownloadLinks;
	public Page_ContentManager()
	{
		RefreshButton = new Button()
		{
			Content = "Refresh",
			HorizontalAlignment = HorizontalAlignment.Center,
		};
		DeleteButton = new()
		{
			Content = "Delete",
			IsEnabled = false,
			HorizontalAlignment = HorizontalAlignment.Center,
		};
		DownloadButton = new()
		{
			Content = "Download",
			IsEnabled = false,
			HorizontalAlignment = HorizontalAlignment.Center,
		};
		DownloadLinks = new();
		SeeInstalled = new ToggleSwitch()
		{
			Content = "See Installed",
			IsChecked = false,
			HorizontalAlignment = HorizontalAlignment.Center,
		};
		LoadingBar = new ProgressBar()
		{
			IsEnabled = false,
			IsIndeterminate = false,
			ShowProgressText = false,
			Value = 0,
			Minimum = 0,
			Maximum = 100,
			HorizontalAlignment = HorizontalAlignment.Stretch
		};
		LinkList = new ListBox()
		{
			VerticalAlignment = VerticalAlignment.Stretch,
			HorizontalAlignment = HorizontalAlignment.Stretch,
			ItemTemplate = new ContentItem(),
		};
		ScrollViewer.SetVerticalScrollBarVisibility(LinkList, Avalonia.Controls.Primitives.ScrollBarVisibility.Visible);
		var col0 = new StackPanel()
		{
			Children =
			{
				SeeInstalled,
				RefreshButton,
				DownloadButton,
				DeleteButton,
				LoadingBar,
			},
			Orientation = Orientation.Vertical,
			Margin = new(5),
			Spacing = 5,
		};
		var grid = new Grid()
		{
			HorizontalAlignment = HorizontalAlignment.Stretch,
			VerticalAlignment = VerticalAlignment.Stretch,
			Children = 
			{
				col0, LinkList
			}
		};
		grid.RowDefinitions.Add(new(GridLength.Star));
		grid.RowDefinitions.Add(new(GridLength.Star));
		grid.ColumnDefinitions.Add(new(GridLength.Star));
		Grid.SetRow(LinkList, 1);
		Content = grid;
		RefreshButton.Tapped += RefreshOnlineLinks;
		SeeInstalled.PropertyChanged += SeeInstalledChanged;
		LinkList.SelectionChanged += OnSelect;
		DownloadButton.Tapped += OnDownloadTapped;
		DeleteButton.Tapped += OnDeleteTapped;
	}
	public void OnDeleteTapped(object? o, TappedEventArgs e)
	{
		var item = LinkList.SelectedItem;
		MainView? inst = MainView.CurrentInstance;
		if (o != DeleteButton || item == null || inst == null) return;
		CoreAionianApp app = inst.CoreApp;
		BibleLink link = (BibleLink)item;
		app.Delete(link);
		inst.Page_BibleReading.UpdateBibles(o, e);
		LinkList.ItemsSource = app.Installed;
	}
	public async void OnDownloadTapped(object? o, TappedEventArgs e)
	{
		var item = LinkList.SelectedItem;
		MainView? inst = MainView.CurrentInstance;
		if (o != DownloadButton || item == null || inst == null) return;
		ProgressMessageHandler handler = new(new HttpClientHandler());
		handler.HttpReceiveProgress += (o, e) => Dispatcher.UIThread.Post(() => LoadingBar.Value = e.ProgressPercentage);
		CoreAionianApp app = inst.CoreApp;
		DownloadButton.IsEnabled = false;
		BibleLink link = (BibleLink)item;
		DownloadLinks.Remove(link);
		Task task = Task.Run(() => app.DownloadBibleAsync(link, handler));
		LinkList.IsEnabled = false;
		SeeInstalled.IsEnabled = false;
		RefreshButton.IsEnabled = false;
		await task;
		LinkList.IsEnabled = true;
		RefreshButton.IsEnabled = true;
		SeeInstalled.IsEnabled = true;
		inst.Page_BibleReading.UpdateBibles(o, e);
	}
	public void OnSelect(object? o, SelectionChangedEventArgs e)
	{
		if (e.Source != LinkList) return;
		if (LinkList.SelectedIndex == -1)
		{
			DownloadButton.IsEnabled = false;
			DeleteButton.IsEnabled = false;
		}
		else if (SeeInstalled.IsChecked.HasValue && SeeInstalled.IsChecked.Value) DeleteButton.IsEnabled = true;
		else DownloadButton.IsEnabled = true;
	}
	public async void RefreshOnlineLinks(object? o, TappedEventArgs e)
	{
		if (SeeInstalled.IsChecked.HasValue && SeeInstalled.IsChecked.Value) return;
		LoadingBar.IsIndeterminate = true;
		LinkList.ItemsSource = null;
		MainView? curinst = MainView.CurrentInstance;
		if (curinst == null) return;
		var instlist = curinst.CoreApp.Installed;
		Task<ObservableCollection<BibleLink>> getlist = Task.Run(GetUpdatedList);
		// LoadingBar.IndeterminateStartingOffset = 0.1;
		// LoadingBar.IndeterminateEndingOffset = 0.9;
		ObservableCollection<BibleLink> GetUpdatedList()
		{
			var items = BibleLink.GetAllUrlsFromWebsite();
			var list = items.
				Select((x) => x.Link).
				Except(instlist);
			return new ObservableCollection<BibleLink>(list);
		}
		DownloadLinks = await getlist;
		LinkList.ItemsSource = DownloadLinks;
		LoadingBar.IsIndeterminate = false;
	}
	public void SeeInstalledChanged(object? o, Avalonia.AvaloniaPropertyChangedEventArgs e)
	{
		var curinst = MainView.CurrentInstance;
		if (e.Sender != SeeInstalled || curinst == null || e.Property != ToggleSwitch.IsCheckedProperty) return;
		bool? checked_val = SeeInstalled.IsChecked;
		if (checked_val == null) return;
		LinkList.ItemsSource = checked_val == false ? DownloadLinks : curinst.CoreApp.Installed;
		RefreshButton.IsEnabled = checked_val == false;
	}
}