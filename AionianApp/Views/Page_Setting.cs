using Avalonia.Controls;
using Avalonia.Media;
using Avalonia.Layout;
using AionianApp.Models;
using System;
using Avalonia.Interactivity;
using Avalonia.Themes.Fluent;
using Avalonia.Styling;

namespace AionianApp.Views;

public class Page_Setting : UserControl
{
	private readonly TextBlock _exampleText;
	internal readonly Slider TextsizeSlider;
	public ComboBox ThemeBox;
	private readonly Button _applyButton;
	public Page_Setting()
	{
		VerticalAlignment = VerticalAlignment.Stretch;
		HorizontalAlignment = HorizontalAlignment.Stretch;
		AppSetting? setting = (MainView.CurrentInstance?.CoreApp.Setting) ?? throw new ArgumentException("Unable to load the settings!");
		TextsizeSlider = new() { Minimum = 5, Maximum = 48, Value = setting.Value.FontSize };
		ThemeBox = new()
		{
			AutoScrollToSelectedItem = true,
			ItemsSource = Enum.GetNames<AppSetting.Theme_Key>(),
			SelectedIndex = (byte)setting.Value.CurrentTheme,
			HorizontalAlignment = HorizontalAlignment.Center
		};
		_exampleText = new() { Text = "This is an example Text!", FontSize = TextsizeSlider.Value, HorizontalAlignment = HorizontalAlignment.Center };
		_applyButton = new() { Content = "Apply", HorizontalAlignment = HorizontalAlignment.Center };
		Margin = new(15);
		Content = new StackPanel()
		{
			VerticalAlignment = VerticalAlignment.Stretch,
			HorizontalAlignment = HorizontalAlignment.Stretch,
			Children =
			{
				new TextBlock() { Text="Text size" },
				TextsizeSlider,
				new Separator(),
				new StackPanel()
				{
					Orientation = Orientation.Horizontal,
					Children =
					{
						new TextBlock() { Text="Theme selector" },
						ThemeBox,
					},
					Margin = new(15),
					Spacing = 30,
				},
				new Separator(),
				_exampleText,
				_applyButton,
			},
			Margin = new(15),
			Spacing = 15,
		};
		TextsizeSlider.PropertyChanged += (o, e) => _exampleText.FontSize = TextsizeSlider.Value;
		_applyButton.Click += OnApply;
	}
	public void OnApply(object? o, RoutedEventArgs e)
	{
		MainView? inst = MainView.CurrentInstance;
		if (e.Source != _applyButton || inst == null || o == null || o != _applyButton) return;
		var prevtheme = inst.CoreApp.Setting.CurrentTheme;
		var idx = ThemeBox.SelectedIndex;
		var new_theme = (AppSetting.Theme_Key)idx;
		inst.CoreApp.ChangeSetting(new() { CurrentTheme = (AppSetting.Theme_Key)ThemeBox.SelectedIndex, FontSize = TextsizeSlider.Value });
		if (prevtheme != new_theme && App.Current is App appinst)
		{
			appinst.ApplyTheme(new_theme);
			inst.Page_BibleReading.UpdateBibles(o, e);
			if (ThemeBox.Parent is StackPanel panel)
			{
				panel.Children.RemoveAt(1);
				ThemeBox = new()
				{

					AutoScrollToSelectedItem = true,
					ItemsSource = Enum.GetNames<AppSetting.Theme_Key>(),
					SelectedIndex = idx,
					HorizontalAlignment = HorizontalAlignment.Center
				};
				panel.Children.Add(ThemeBox);
				inst.Page_BibleReading.LoadComboBoxes();
			}
		}
		inst.CurrentTabs.SelectedIndex = 0;
	}
}