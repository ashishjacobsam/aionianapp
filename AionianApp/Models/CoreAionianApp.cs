using System;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;

using Aionian;
namespace AionianApp.Models
{
	public class CoreAionianApp : CoreApp
	{
		internal const string SettingFileName = "Settings.json";
		public AppSetting Setting {get; protected set;}
		public event EventHandler<AppSetting.AppSettingArgs>? SettingChanged;
		public CoreAionianApp() : base()
		{
			Setting = File.Exists(AssetFilePath(SettingFileName))
				? LoadFileAsJson<AppSetting>(SettingFileName)
				: (new(){CurrentTheme = AppSetting.Theme_Key.Dark, FontSize = 15});
		}
		public BibleLink[] Installed => AvailableBibles.ToArray();
		public int InstalledCount => AvailableBibles.Count;
		public void DownloadBible(BibleLink link, HttpMessageHandler? handler)
		{
			StreamReader stream = link.DownloadStreamAsync(handler).Result;
			Bible bible = Bible.ExtractBible(stream);
			SaveFileAsJson(bible, AssetFileName(bible));
			AvailableBibles.Add(link);
			SaveAssetLog();
		}
		public ChapterwiseBible GetReadingMaterial(BibleLink link) => new(LoadFileAsJson<Bible>(AssetFileName(link)));
		public void ChangeSetting(AppSetting newsetting)
		{
			Setting = newsetting;
			SaveFileAsJson(newsetting, SettingFileName);
			SettingChanged?.Invoke(this, new(){Cur = newsetting});
		}
	}
}