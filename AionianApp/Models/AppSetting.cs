using System;

namespace AionianApp.Models
{
	public struct AppSetting
	{
		public enum Theme_Key : byte { Light, Dark, NeoLight, NeoDark }
		public Theme_Key CurrentTheme;
		public double FontSize;
		public class AppSettingArgs : EventArgs
		{
			public AppSetting Cur;
		}
	}
}