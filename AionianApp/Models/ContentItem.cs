using Aionian;

using Avalonia.Controls;
using Avalonia.Controls.Templates;

namespace AionianApp.Models;

public class ContentItem : IDataTemplate
{
	public bool Match(object? data)
	{
		return data != null && data is BibleLink;
	}

	Control? ITemplate<object?, Control?>.Build(object? param)
	{
		if (param == null || (param is BibleLink) == false) return null;
		BibleLink link = (BibleLink)param;
		return new TextBlock()
		{
			Text = $"{link.Language} Bible, {link.Title}",
			HorizontalAlignment = Avalonia.Layout.HorizontalAlignment.Center,
			TextAlignment = Avalonia.Media.TextAlignment.Center,
			TextWrapping = Avalonia.Media.TextWrapping.WrapWithOverflow,
		};
	}
}