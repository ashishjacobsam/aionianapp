﻿using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using Avalonia.Android;

namespace AionianApp.Android;

[Activity(Label = "AionianApp.Android", Theme = "@style/MyTheme.NoActionBar", Icon = "@drawable/icon", LaunchMode = LaunchMode.SingleTop, Immersive = true, ScreenOrientation = ScreenOrientation.FullUser, ConfigurationChanges = ConfigChanges.KeyboardHidden | ConfigChanges.ScreenLayout)]
public class MainActivity : AvaloniaMainActivity
{
	protected override void OnResume()
	{
		int uiOptions = (int)Window.DecorView.SystemUiVisibility;
		uiOptions |= (int)SystemUiFlags.Fullscreen;
		uiOptions |= (int)SystemUiFlags.HideNavigation;
		uiOptions |= (int)SystemUiFlags.ImmersiveSticky;
		uiOptions |= (int)SystemUiFlags.LayoutHideNavigation;
		uiOptions |= (int)SystemUiFlags.LayoutFullscreen;
		uiOptions |= (int)SystemUiFlags.LayoutStable;
		WindowManagerLayoutParams lp = Window.Attributes;
		lp.Flags = WindowManagerFlags.Fullscreen;
		lp.SystemUiVisibility = (StatusBarVisibility)uiOptions;

		base.OnResume();
	}
	public override void OnBackPressed() 
	{
	} // Do Nothing
}
